# -*- utf-8 -*-
# @Time:2021/4/21 0021 8:53
# @Author: zhaowangfa
# @file http_requests.py
# http请求

import requests
from tool.do_logs import Dolog


class HttpRequests:

    def http_requests(self, url, data, http_method, cookie=None):
        try:
            if http_method.upper() == 'GET':
                res = requests.get(url, data, cookies=cookie)
                return res  # 返回结果
            elif http_method.upper() == 'POST':
                res = requests.post(url, data, cookies=cookie)
                return res  # 返回结果
            else:
                print('输入的请求不对')

        except Exception as e:
            Dolog().error(e)
            # print('get请求报错了{0}'.format(e))
            raise e  # 抛出异常


if __name__ == '__main__':
    # 注册
    register_url = 'http://119.23.241.154:8080/futureloan/mvc/api/member/register'
    register_data = {'mobilephone': '15096044433', 'pwd': '123456', 'regname': 'huahua'}
    # register_res = requests.get(register_url, params=register_data)
    # print('text的结果解析', register_res.text)
    # print('json的结果解析', register_res.json())

    # 登录
    login_url = 'http://119.23.241.154:8080/futureloan/mvc/api/member/login'
    login_data = {'mobilephone': '15096044433', 'pwd': '123456'}
    # login_res = requests.post(login_url, login_data)
    # print('text的结果解析', login_res.text)
    # print('json的结果解析', login_res.json())

    # 充值
    recharge_url = 'http://119.23.241.154:8080/futureloan/mvc/api/member/recharge'
    recharge_data = {'mobilephone': '15096044433', 'amount': '100'}
    # recharge_res = requests.get(recharge_url, params=recharge_data)
    # print('text的结果解析', recharge_res.text)
    # print('json的结果解析', recharge_res.json())

    register_res = HttpRequests().http_requests(register_url, register_data, 'get')
    login_res = HttpRequests().http_requests(login_url, login_data, 'post')
    recharge_res = HttpRequests().http_requests(recharge_url, recharge_data, 'post', login_res.cookies)
