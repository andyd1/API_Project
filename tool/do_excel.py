# -*- utf-8 -*-
# @Time:2021/4/21 0021 8:52
# @Author: zhaowangfa
# @file do_excel.py
# 处理Excel表格数据
from openpyxl import load_workbook
from config.read_config import ReadConfig
from tool.get_path import *


class DoExcel:

    # 读取Excel表单里的数据
    def get_data(self, file_name):
        wb = load_workbook(file_name)   # 加载Excel表格
        mode = eval(ReadConfig.get_config(config_path, 'MODE', 'mode'))
        test_data = []
        for key in mode:
            sheet = wb[key]  # 读取Excel的那个表单
            if key == 'all':
                for i in range(2, sheet.max_row+1):  # 读取第二行的所有字段数据
                    row_data = {}
                    row_data['case_id'] = sheet.cell(i, 1).value
                    row_data['url'] = sheet.cell(i, 2).value
                    row_data['data'] = sheet.cell(i, 3).value
                    row_data['title'] = sheet.cell(i, 4).value
                    row_data['http_method'] = sheet.cell(i, 5).value
                    row_data['expected'] = sheet.cell(i, 6).value
                    row_data['sheet_name'] = key
                    test_data.append(row_data)
            else:
                for case_id in wb[key]:
                    row_data = {}
                    row_data['case_id'] = sheet.cell(case_id + 1, 1).value
                    row_data['url'] = sheet.cell(case_id + 1, 2).value
                    row_data['data'] = sheet.cell(case_id + 1, 3).value
                    row_data['title'] = sheet.cell(case_id + 1, 4).value
                    row_data['http_method'] = sheet.cell(case_id + 1, 5).value
                    row_data['expected'] = sheet.cell(case_id + 1, 6).value
                    row_data['sheet_name'] = key
                    test_data.append(row_data)

        # print(test_data)  # 获取Excel的单元格值
        return test_data

    # 把数据写入Excel表单里
    def write_back(self, file_name, sheet_name, i, value_1, value_2):
        wb = load_workbook(file_name)   # 加载Excel表格
        # mode = eval(ReadConfig.get_config(config_path, 'MODE', 'mode'))
        # for key in mode:
        sheet = wb[sheet_name]
        sheet.cell(i, 7).value = str(value_1)
        sheet.cell(i, 8).value = value_2
        wb.save(file_name)


if __name__ == '__main__':
    DoExcel().get_data('../test_data/test_data.xlsx')
