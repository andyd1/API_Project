# -*- utf-8 -*-
# @Time:2021/4/21 0021 21:03
# @Author: zhaowangfa
# @file get_path.py
# 获取文件路径

import os
import datetime

time = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

# 项目根目录
path = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]
# print(path)

# 测试用例数据的路径
test_data_path = os.path.join(path, 'test_data', 'test_data.xlsx')
# print(test_data_path)

# 测试报告路径
test_report_path = os.path.join(path, 'test_result', 'report', time, '.html')
# print(test_report_path)

# 配置文件路径
config_path = os.path.join(path, 'config', 'case.config')

# 日志路径
log_path = os.path.join(path, 'test_result', 'log', '{0}.txt'.format(time))
print(log_path)

