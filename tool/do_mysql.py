# -*- utf-8 -*-
# @Time:2021/4/24 0024 22:57
# @Author: zhaowangfa
# @file do_mysql.py
import mysql.connector
from tool.get_path import config_path
from config.read_config import ReadConfig


class DoMysql:

    def do_mysql(self, state='all'):
        # 创建一个数据库连接
        db_config = eval(ReadConfig.get_config(config_path, 'DB', 'db_config'))
        cn = mysql.connector.connect(**db_config)

        # 游标
        cursor = cn.cursor()
        # sql语句
        query_sql = 'select * from b where phone = 15374844848'
        # 执行
        cursor.execute(query_sql)
        # 获取结果 打印结果
        if state == 'all':
            res = cursor.fetchone()  # 元组形式输出数据，针对的是一条数据
        else:
            res = cursor.fetchall()  # 列表嵌套元组形式输出数据，获取多条数据时用这个
        print(res)

        # 关闭游标
        cursor.close()

        # 关闭连接
        cn.close()
        return res
