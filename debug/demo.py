# -*- utf-8 -*-
# @Time:2021/4/21 0021 16:47
# @Author: zhaowangfa
# @file demo.py
# 练习请求语法
from ddt import ddt, data
# import requests
# url = 'https://www.gaoding.com/api/aggregate/search'
# data = 'q=nn&page_size=120&page_num=1&design_cid=&channel_cid=&industry_cid=&filter_id=681%2C681&type_filter_id=&channel_filter_id=&channel_children_filter_id=&sort=&styles=&colors=&ratios='
# res = requests.get(url, params=data)
# print(res.text)
# print(res.json())

# from tool.do_excel import DoExcel
import unittest
# test_data = DoExcel().get_data('D:/API_Project/test_data/test_data.xlsx', 'login')
test_data = [1, 3]


@ddt
class TestMath(unittest.TestCase):

    @data(test_data)
    def test_do(self, item):
        print('item:', item)
