# -*- utf-8 -*-
# @Time:2021/4/21 0021 16:58
# @Author: zhaowangfa
# @file test_http_requests.py
# 接口测试用例
import unittest
from tool.http_requests import HttpRequests
from ddt import ddt, data
from tool.do_excel import DoExcel
from tool.get_path import *

test_data = DoExcel().get_data(test_data_path)
cookie = None


@ddt
class TestHttpRequests(unittest.TestCase):

    def setUp(self):
        pass

    @data(*test_data)
    def test_api(self, item):

        global cookie
        test_result = ''
        print('正在进行测试的是{0}'.format(item['title']))
        res = HttpRequests().http_requests(item['url'], eval(item['data']), item['http_method'], cookie)
        if res.cookies:
            cookie = res.cookies

        # 判断测试结果
        try:
            self.assertEqual(str(item['expected']), res.json()['code'])
            test_result = 'Pass'
        except Exception as e:
            test_result = 'Failed'
            raise e
        finally:
            # 写入测试结果
            DoExcel().write_back(test_data_path, item['sheet_name'], item['case_id'] + 1, res.json(), test_result)
            print('测试结果是{0}'.format(res.json()))

    def tearDown(self):
        print('这是结束函数')
        pass
