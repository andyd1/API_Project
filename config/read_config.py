# -*- utf-8 -*-
# @Time:2021/4/21 0021 22:24
# @Author: zhaowangfa
# @file read_config.py

import configparser


class ReadConfig:

    @staticmethod
    def get_config(file_path, section, option):
        cf = configparser.ConfigParser()
        cf.read(file_path)
        return cf[section][option]


if __name__ == '__main__':
    from tool.get_path import *
    print(ReadConfig.get_config(config_path, 'MODE', 'mode'))
    print(eval(ReadConfig.get_config(config_path, 'MODE', 'mode')))
