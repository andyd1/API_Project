# -*- utf-8 -*-
# @Time:2021/4/21 0021 12:31
# @Author: zhaowangfa
# @file run.py
import unittest
from test_case.test_http_requests import TestHttpRequests
import HTMLTestRunner
from tool.get_path import *

# cookie = None
#
#
# def run(data, sheet_name):
#     global cookie
#     for item in data:
#         print('正在进行测试的是{0}'.format(item['title']))
#         res = HttpRequests().http_requests(item['url'], eval(item['data']), item['http_method'], cookie)
#         if res.cookies:
#             cookie = res.cookies
#
#         print('测试结果是{0}'.format(res.json()))
#         # 写入测试结果
#         DoExcel().write_back('../test_data/test_data.xlsx', sheet_name, item['case_id'] + 1, res.json())
#
#
# # 加载Excel，读取测试数据
# test_data = DoExcel().get_data('../test_data/test_data.xlsx', 'login')
# run(test_data, 'login')

suite = unittest.TestSuite()
# suite.addTest(TestHttpRequests('test_api'))
loader = unittest.TestLoader()
suite.addTest(loader.loadTestsFromTestCase(TestHttpRequests))

with open(test_report_path, 'wb') as file:
    runner = HTMLTestRunner.HTMLTestRunner(stream=file,
                                           verbosity=2,
                                           title='数据运算单元测试报告',
                                           description='duke执行的单元测试',
                                           tester='duke')
    runner.run(suite)
